/*
function runExpression()
{
	var a=10;
	
	function add(b){
		return a+b;
	}
	console.log( add(90), add(20) );
}
*/


/**
 * This -> contiene un objecto, un array y una funcion.
 */


/*
var object = {
	prop:this,
	method: function(){ return this; }

};

var array = [
	this,
	function(){ return this; }
];

function global(){
	console.log('FROM GLOBAL', this);
	function sub() { console.log('from sub', this ) }
	sub();
}

//global.call(object);
//new global();
*/


//PROTOTIPOS EN OBJETOS
function Apple(x,y,color,){
	this.color=color;
	this.x=x;
	this.y=y;
	
}

//crear los prototipos.
Apple.prototype = {
	eat: 	function(){ return "funcion de comer";},
	throw: 	function(){ return "throw apple"; }
};

//sobreescribir una función:
Apple.proptorype.eat = function(){return "nueva funcion de comer"};

