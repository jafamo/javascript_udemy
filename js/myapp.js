var a = document-getElementById('Hello');
a.innerText = "nuevo Mundo";
a.innerHTML += "nuevo mundo";
a.outerHTML = '<h2 id="miId"> Mensaje de pruebas </h2>';

// Esto significa que selecciona todos los elementos con h1 span id clase
// querySelectorAll --> devuelve un array
// querySelector --> devuelve el primer elemento resultante
// source:
// http://xitrus.es/blog/112/Seleccionar_elementos_con_querySelector_de_JavaScript
var spanH1 = document.querySelectorAll('h1 span #id  .clase');


spanH1.innerHTML = "nuevo texto en negrita";


console.log('http://xitrus.es/blog/112/Seleccionar_elementos_con_querySelector_de_JavaScript');