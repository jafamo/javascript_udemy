var select = document.getElementsByName('coches')[0];

select.onclick = function(){
	console.log( );

};

function ClickCallback(event){
	console.log('click para añadir el evento.');
}

select.addEventListener('click', ClickCallback);

/*
select.addEventListener('click', function(  ){

	console.log('click para añadir el evento 1.');
});
*/
select.removeEventListener('click', ClickCallback);

/*Creamos nuevos elementos.
//Creamos un nuevo elmento de tipo div con unos css y con una funcion 
al hacer click sobre ese elemento se hace un alert. 
*/

var element = document.createElement('div');
element.style.cssText = "width:200px; height:200px; background:blue;";
element.onclick = function() { alert('hello'); };

document.body.appendChild(element);