//obtenemos el elmento con id=style.

var el = document.getElementById('style');

/*
1.- Forma de modificar el CSS

el.style.background = "blue";
el.style.color = "white";
el.style.width = "200px";
*/

/**
 * Modificar el CSS en una linea
 */
el.style.cssText = "background:blue; color:yellow";